var cat1 = {
  name: "mun",
  age: 2,
  speak: function () {
    console.log("meo meo", this.name);
  },
};
// var varibale={
//  key:value
// }
// array => index ~ có thứ tự
// object => key ~ ko có thứ tự
// truy xuất value qua key

var catName = cat1.name;

// update value cho key

cat1.name = "bull";
console.log(`  🚀: cat1`, cat1);
cat1.speak();

// var catAge = cat1.age;
var catAge = cat1["age"];

var key = "age";
var value = "20";
cat1[key] = value;
console.log(`  🚀: cat1`, cat1);

// pass by value : string,number,boolean, pass by rererence : array object

var a = 2;
var b = a;
b = 5;
console.log(`  🚀: a`, a);

var cat2 = cat1;
console.log(`  🚀: cat1`, cat1);
console.log(`  🚀: cat2`, cat2);

cat2.name = "Mực";
console.log(`  🚀: cat1`, cat1);
console.log(`  🚀: cat2`, cat2);

var dog1 = {
  name: "Quách đen",
  age: 2,
};

var dog2 = {
  ten: "Quách mực",
  tuoi: 2,
};
console.log(`  🚀: dog2`, dog2);

function Dog(_name, _age) {
  this.name = _name;
  this.age = _age;
  this.speak = function () {
    console.log("Gâu gâu tao là :", this.name);
  };
}

var dog3 = new Dog("Quách xanh", 3);
var dog4 = new Dog("Quách vàng", 6);
dog3.speak();
console.log(`  🚀: dog4`, dog4);
console.log(`  🚀: dog3`, dog3);
