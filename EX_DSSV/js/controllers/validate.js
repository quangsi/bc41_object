// hợp lệ => return true

function kiemTraTrung(idSV, svArr) {
  // findIndex return vị trí của item nếu điều kiện true, nếu ko tìm thấy sẽ return -1
  var viTri = svArr.findIndex(function (item) {
    return item.maSV == idSV;
  });
  if (viTri != -1) {
    //
    document.getElementById("spanMaSV").innerText = "Mã sinh viên đã tồn tại";
    return false;
  } else {
    document.getElementById("spanMaSV").innerText = "";
    return true;
  }
}

function kiemTraDoDai(value, idErr, min, max) {
  var length = value.length;

  if (length < min || length > max) {
    document.getElementById(
      idErr
    ).innerText = `Độ dài phải từ ${min} đến ${max} kí tự`;
    return false;
  } else {
    document.getElementById(idErr).innerText = "";
    return true;
  }
}

function kiemTraEmail(value) {
  const re =
    /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
  var isEmail = re.test(value);
  if (isEmail) {
    document.getElementById("spanEmailSV").innerText = "";
    return true;
  } else {
    document.getElementById("spanEmailSV").innerText =
      "Email không đúng định dạng";
    return false;
  } 
}
function kiemTraSo(value) {
  var re = /^\d+$/;
  var isNumber = re.test(value);
  if (isNumber) {
    document.getElementById("spanMaSV").innerText = "";
    return true;
  } else {
    document.getElementById("spanMaSV").innerText =
      "Mã sinh viên chỉ bao gồm số";
    return false;
  }
}
