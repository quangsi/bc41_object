function layThongTinTuForm() {
  // lấy thông tin từ form
  var _maSV = document.getElementById("txtMaSV").value;
  var _tenSV = document.getElementById("txtTenSV").value;
  var _emailSV = document.getElementById("txtEmail").value;
  var _matKhauSV = document.getElementById("txtPass").value;
  var _diemToan = document.getElementById("txtDiemToan").value;
  var _diemLy = document.getElementById("txtDiemLy").value;
  var _diemHoa = document.getElementById("txtDiemHoa").value;
  // tạo object sv
  return new SinhVien(
    _maSV,
    _tenSV,
    _emailSV,
    _matKhauSV,
    _diemToan,
    _diemLy,
    _diemHoa
  );
}
function renderDSSV(svArr) {
  // render danh sách sinh viên
  // contentHTML ~ chuỗi chứa các thẻ tr
  var contentHTML = "";
  for (var index = 0; index < svArr.length; index++) {
    var sv = svArr[index];
    var contentTr = ` <tr>
                        <td>${sv.maSV}</td>
                        <td>${sv.tenSV}</td>
                        <td>${sv.emailSV}</td>
                        <td>${sv.tinhDTB()}</td>
                        <td>
                        <button onclick="xoaSV('${
                          sv.maSV
                        }')" class="btn btn-danger">Xoá</button>
                        <button onclick="suaSV('${
                          sv.maSV
                        }')" class="btn btn-warning">Sửa</button>
                        </td>
                    </tr> `;
    contentHTML = contentHTML + contentTr;
  }
  // show ra table
  document.getElementById("tbodySinhVien").innerHTML = contentHTML;
}

function timKiemViTri(id, arr) {
  var viTri = -1;
  for (var index = 0; index < arr.length; index++) {
    if (arr[index].maSV == id) {
      viTri = index;
    }
  }
  return viTri;
}
