var DSSV = [];
// lấy dữ liệu từ localStorage khi user load trang

let dataJson = localStorage.getItem("DSSV_LOCAL");
console.log(`  🚀: dataJson`, dataJson);
// kiểm tra nếu có data ở localStorage thì mới gán vào array DSSV
if (dataJson != null) {
  var dataArr = JSON.parse(dataJson);
  // dữ liệu lấy lên từ localStorage sẽ bị mất method => từ array ko có method tinhTDB => convert thành array có mehtod tinhTDB bằng map()
  DSSV = dataArr.map(function (item) {
    var sv = new SinhVien(
      item.maSV,
      item.tenSV,
      item.emailSV,
      item.matKhauSV,
      item.diemToan,
      item.diemLy,
      item.diemHoa
    );
    return sv;
  });
  console.log(`  🚀: DSSV`, DSSV);
  renderDSSV(DSSV);
}
// thêm sv
function themSV() {
  var sv = layThongTinTuForm();
  // validate ~ kiểm tra dữ liệu
  var isValid = true;
  isValid =
    kiemTraSo(sv.maSV) &&
    kiemTraTrung(sv.maSV, DSSV) &&
    kiemTraDoDai(sv.maSV, "spanMaSV", 6, 8);
  // & : phép cộng bit
  isValid = isValid & kiemTraEmail(sv.emailSV);
  if (isValid) {
    // push sv vào DSSV
    DSSV.push(sv);

    //  convert array DSSV thành json
    var dssvJson = JSON.stringify(DSSV);
    // lưu json vào localStorage
    localStorage.setItem("DSSV_LOCAL", dssvJson);
    renderDSSV(DSSV);
  }
}
// xoá sv

function xoaSV(idSV) {
  // splice(viTri,1)
  var viTri = timKiemViTri(idSV, DSSV);
  if (viTri != -1) {
    DSSV.splice(viTri, 1);
    renderDSSV(DSSV);
  }
}

function suaSV(idSV) {
  var viTri = timKiemViTri(idSV, DSSV);
  if (viTri != -1) {
    var sv = DSSV[viTri];
    console.log(`  🚀: suaSV -> sv`, sv);
    document.getElementById("txtMaSV").disabled = true;
  }
}

// var color = ["red", "blue"];
// var newColor = color.map(function (item) {
//   return "alice " + item;
// });
// console.log(`  🚀: newColor`, newColor);
