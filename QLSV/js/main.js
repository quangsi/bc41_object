function hienThiThongTin() {
  // lấy thông tin từ form
  var _maSv = document.getElementById("txtMaSV").value;
  var _tenSv = document.getElementById("txtTenSV").value;
  var _loai = document.getElementById("loaiSV").value;
  var _diemToan = document.getElementById("txtDiemToan").value * 1;
  var _diemVan = document.getElementById("txtDiemVan").value * 1;
  console.log({
    _maSv,
    _tenSv,
    _loai,
    _diemToan,
    _diemVan,
  });

  // tạo object sv
  var sv = {
    maSv: _maSv,
    tenSv: _tenSv,
    loaiSv: _loai,
    diemToan: _diemToan,
    diemVan: _diemVan,
    tinhDTB: function () {
      return (this.diemToan + this.diemVan) / 2;
    },
    xepLoai() {
      var diem = this.tinhDTB();
      if (diem >= 5) {
        return "Đạt";
      } else {
        return "Rớt";
      }
    },
  };
  // show thông tin
  document.getElementById("spanMaSV").innerText = sv.maSv;
  document.getElementById("spanTenSV").innerText = sv.tenSv;
  document.getElementById("spanLoaiSV").innerText = sv.loaiSv;
  document.getElementById("spanDTB").innerText = sv.tinhDTB();
  document.getElementById("spanXepLoai").innerText = sv.xepLoai();
}
